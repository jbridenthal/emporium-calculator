/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jyncsed.gmail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author James
 */
public class VersionCheck {
    
    private String version = "3.1";
    private boolean upToDate = true;
    VersionCheck(){
        
    }
    
    public boolean checkCurrentVersion() throws MalformedURLException, IOException {
        String currentVersion = this.getCurrentVersion();
        if(currentVersion.contentEquals(version)){
            upToDate = true;
        }
        else
            upToDate = false;
        
        return upToDate;
    }

    private BufferedReader getCurrentVersionFromWebsite() throws IOException, MalformedURLException {
        String superurl = "http://pastebin.com/raw.php?i=9Lr9KKFM";
        URLConnection urlConn = getURLInformation(superurl);
        InputStreamReader inStream = new InputStreamReader(urlConn.getInputStream());
        BufferedReader buff = new BufferedReader(inStream);
        return buff;
    }

    private URLConnection getURLInformation(String superurl) throws IOException, MalformedURLException {
        URL url = new URL(superurl);
        URLConnection urlConn = url.openConnection();
        urlConn.setConnectTimeout(5000);
        urlConn.setReadTimeout(5000);
        return urlConn;
    }

    
    public String getThisVersion() {
        return version;
    }
    public String getCurrentVersion() throws IOException {
        String currentVersion = "";
        BufferedReader buff = getCurrentVersionFromWebsite();
        currentVersion = buff.readLine();
        return currentVersion;
    }
            
    
    
}
