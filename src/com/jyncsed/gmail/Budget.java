/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jyncsed.gmail;

/**
 *
 * @author James
 */
public class Budget {
    public Budget()
    {
        
    }
    private double tax = 0;
    private double sell = 0;
    private double buy = 0;
    private double current = 0;
    private double balance = 0;
    private double tax_percent = .05;
    
    public double getTax(){
        return this.tax;
    }
    public double getSellTotal() {
        return this.sell;
    }
    public void addSellPrice(double value) {
        this.sell += value;
    }
    public void setSellPrice(double value) {
        this.sell += value;
    }
    
    public double getBuyTotal() {
        return this.buy;
    }
    
    public void addBuyPrice(double value) {
        this.buy += value;
    }
    public void setBuyPrice(double value) {
        this.buy = value;
    }
    public void changeCurrentTotal(double value) {
        this.current = value;
    }
    public double getCurrentBalance() {
        return this.balance;
    }
    public void setBalance(double value) {
        this.balance = value;
    }
    public void setCurrent(double value) {
        this.current = value;
    }
    public void setTax(double value) {
        this.tax = value;
    }
    public void clearValues() {
        this.tax = 0;
        this.balance = 0;
        this.buy = 0;
        this.current = 0;
        this.sell = 0;
              
    }
    public double calculateTax() {
        this.tax = this.sell * this.tax_percent;
        return this.tax;
    }
    
    public double calculateTotal()
    {
        this.balance = this.current + this.buy + this.sell - this.tax;
        
        return this.balance;
    }
}
