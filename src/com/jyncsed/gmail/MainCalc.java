/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jyncsed.gmail;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author James
 */
public class MainCalc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, IOException {
        // TODO code application logic here
        
        try
        {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }
        catch(Exception ex)
        {
            
        }
      VersionCheck vc = new VersionCheck();
        if(!vc.checkCurrentVersion()) {
               JOptionPane.showMessageDialog(null, "Emporium Calulator is out of date. \nYour Version is:" + vc.getThisVersion() + "\nCurrent Version is:" + vc.getCurrentVersion() + "\n Please visit http://bit.ly/OfznDf for more details");
        }
        
        MainFrame tframe = new MainFrame();
        tframe.setTitle("Emporium Calculator and Inventory Manager " + vc.getThisVersion() + " by Jyncs");
       tframe.show();
    }
}
