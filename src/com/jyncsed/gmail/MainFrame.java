package com.jyncsed.gmail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author James
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form TestFrame
     */
    public MainFrame() {
        
         try
        {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }
        catch(Exception ex)
        {
            
        }
       initComponents();
        

    }
    Budget budget = new Budget();
    private void AddNewDataToTable(String[][] newTableModel, int x, String itemName, int itemAmount, int buyPrice, int sellPrice) {
        newTableModel[x][0] = itemName;
        newTableModel[x][1] = String.format("%s",itemAmount);
        newTableModel[x][2] = String.format("%s",buyPrice);
        newTableModel[x][3] = String.format("%s",sellPrice);
        tblItems.setModel(new DefaultTableModel(
           newTableModel,
            new String [] {
                "Item", "Amount", "Purchase", "Sold"
            }));
    }
    private void ClearTable() {
        String [][] newTableModel = null;
        tblItems.setModel(new DefaultTableModel(newTableModel, new String [] {"Item","Amount","Purchase","sold"}));
   }
    private int GetItemAmount() {
        Object spinnerval = spnItemAmount.getValue();
        int itemAmount = (Integer) spinnerval;
        if(chkStacks.isSelected())
        {
            itemAmount *= 64;
        }
        return itemAmount;
    }

    private void UpdateBudgetValues(double buyPrice, double sellPrice) throws NumberFormatException {
        budget.changeCurrentTotal(Double.valueOf(this.txtBudgetCurrent.getText()));
        budget.addBuyPrice(buyPrice);
        budget.addSellPrice(sellPrice);
        this.txtBudgetTax.setText(String.valueOf(budget.calculateTax()));
        this.txtBudgetPurchased.setText(String.valueOf(budget.getBuyTotal()));
        this.txtBudgetSold.setText(String.valueOf(budget.getSellTotal()));
        this.txtBudgetBalance.setText(String.valueOf(budget.calculateTotal()));
    }

    private String[][] GetCurrentTableItems(int x, TableModel tableModel) {
        String[][] newTableModel = new String[x+1][4];
        for( int i = 0; i < tableModel.getRowCount();i++)
        {
            newTableModel[i][0] = tableModel.getValueAt(i,0).toString();
            newTableModel[i][1] = tableModel.getValueAt(i,1).toString();
            newTableModel[i][2] = tableModel.getValueAt(i,2).toString();
            newTableModel[i][3] = tableModel.getValueAt(i,3).toString();
        }
        return newTableModel;
    }
    private int GetBuyPrice() {
        VersionCheck vc = new VersionCheck();
        try {
            vc.getCurrentVersion();
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } 
        TreePath[] paths = treeItems.getSelectionPaths();
        String details = paths[0].toString();
        String[] detailsSplit = details.split(",");
        String[] detailsSecondSplit = detailsSplit[2].split(":");
        String[] buyPriceSplit = detailsSecondSplit[1].split(" ");
        int buyPrice = Integer.parseInt(buyPriceSplit[0]);
        return buyPrice;
    }
    private int GetSellPrice() {
        TreePath[] paths = treeItems.getSelectionPaths();
        String details = paths[0].toString();
        String[] detailsSplit = details.split(",");
        String[] detailsSecondSplit = detailsSplit[2].split(":");
        int sellPrice = Integer.parseInt(detailsSecondSplit[2].substring(0, detailsSecondSplit[2].length()-1));
        return sellPrice;
    }
    private String GetItemName() {
        TreePath[] paths = treeItems.getSelectionPaths();
        String details = paths[0].toString();
        String[] detailsSplit = details.split(",");
        String[] detailsSecondSplit = detailsSplit[2].split(":");
        String itemName = detailsSecondSplit[0].substring(0, detailsSecondSplit[0].length() - 4);
        return itemName;
    }
    public void populateTree(DynamicTree treePanel) {
        String blocks = "Building Blocks";
        String organic = "Organic";
        String wood = "Wood/Wood Products";
        String ores = "Ores";
        String wool = "Wool";
        String tools = "Tools/Weapons";
        String stairs = "Stairs";
        String nether = "Nether";
        String brewing = "Brewing";
        String other = "Other/Unsorted";
        String music = "Music";
        String rails = "Rails";
        String redstone = "Redstone";
        String food = "Food";
        String animal = "Animal Products";
        String clay = "Clay";
        String dyes = "Dyes";
        String eggs = "Eggs";
        
        DefaultMutableTreeNode pBlocks, 
                pOrganic, 
                pWood, 
                pOres, 
                pWool, 
                pTools, 
                pStairs, 
                pNether, 
                pBrewing, 
                pOther, 
                pMusic, 
                pRails, 
                pRedstone,
                pFood,
                pAnimal,
                pClay,
                pDyes,
                pEggs;

        pBlocks = treePanel.addObject(null, blocks);
        pOrganic = treePanel.addObject(null, organic);
        pWood = treePanel.addObject(null, wood);
        pOres = treePanel.addObject(null, ores);
        pWool = treePanel.addObject(null, wool);
        pClay = treePanel.addObject(null, clay);
        pStairs = treePanel.addObject(null, stairs);
        pNether = treePanel.addObject(null, nether);
        pDyes = treePanel.addObject(null, dyes);
        pTools = treePanel.addObject(null, tools);
        pBrewing = treePanel.addObject(null, brewing);
        pMusic = treePanel.addObject(null,music);
        pRails = treePanel.addObject(null,rails);
        pRedstone = treePanel.addObject(null,redstone);
        pFood = treePanel.addObject(null,food);
        pAnimal = treePanel.addObject(null,animal);
        pEggs = treePanel.addObject(null, eggs);
        pOther = treePanel.addObject(null, other);

        BufferedReader buff;
        try {
            buff = getBufferReader();
            String line;
            while ((line = buff.readLine()) != null) {
                String[] details = line.split(",");
                if(details.length == 7)
                {
                    int item_id = Integer.parseInt(details[0]);     
                    String lineToAdd = details[6] + " Buy:" + details[4] + " Sell:" + details[5];
                    switch(item_id){
                        //building blocks
                        case 1: case 2: case 3: case 4: case 7:
                        case 12: case 13: case 20: case 24: case 43:
                        case 44: case 45:  case 48: case 49: case 79:
                        case 80: case 82: case 98: case 101: case 102:
                        case 110: case 139:
                            treePanel.addObject(pBlocks,lineToAdd);
                            break;
                        //organic
                        case 6: case 31: case 32: case 37: case 38:
                        case 39: case 40: case 81: case 83: case 86:
                        case 103: case 106: case 111: case 170: case 295:
                        case 296: case 338: case 361: case 362:
                            treePanel.addObject(pOrganic,lineToAdd);
                            break;
                        //wood
                        case 5: case 17: case 18: case 64: case 65: case 85:
                        case 96: case 280: case 281:
                            treePanel.addObject(pWood,lineToAdd);
                            break;
                        //ores
                        case 14: case 15: case 16: case 21: case 22:
                        case 41: case 42: case 56: case 57: case 173:
                        case 263: case 264: case 388: case 265: case 266:
                        case 371:
                            treePanel.addObject(pOres,lineToAdd);
                            break;
                        //tools/weapons
                        case 23: case 26: case 46: case 50: case 54: case 58: 
                        case 61: case 91: case 116: case 130: case 145: case 154:
                        case 158: case 262: case 289: case 302: case 303: case 304:
                        case 305: case 318: case 325: case 326: case 327: case 329:
                        case 347: case 358: case 359: case 386: case 389: case 390:
                           treePanel.addObject(pTools,lineToAdd);
                            break;
                        //stairs
                        case 53: case 67: case 108: case 109: case 134:
                             treePanel.addObject(pStairs,lineToAdd);
                            break;
                        //nether
                        case 47: case 87: case 88: case 89: case 112: case 113:
                        case 114: case 115: case 121: case 155: case 348: case 368:
                        case 369: case 370: case 372: case 381: case 399:
                        case 405: case 406:
                            treePanel.addObject(pNether,lineToAdd);
                            break;
                        //brewing
                        case 373: case 374: case 375: case 377: case 379: case 380:
                            treePanel.addObject(pBrewing,lineToAdd);
                            break; 
                        //food
                        case 260: case 297: case 319: case 320: case 322: 
                        case 335: case 344: case 349: case 350: case 353:
                        case 354: case 357: case 360: case 363: case 364:
                        case 365: case 366: case 391: case 392: case 393: 
                        case 394: case 396: case 400:
                            treePanel.addObject(pFood,lineToAdd);
                            break;
                        //clay
                        case 159: case 172: case 337:
                           treePanel.addObject(pClay,lineToAdd);
                           break;                           
                        //wool
                        case 35: case 287:
                            treePanel.addObject(pWool,lineToAdd);
                            break;
                       //animal
                       case 30: case 288: case 334: case 341: case 352:
                       case 367:
                            treePanel.addObject(pAnimal,lineToAdd);
                            break;
                        //dyes
                        case 351:
                            treePanel.addObject(pDyes,lineToAdd);
                            break;
                        //rails
                        case 27: case 28: case 66: case 157: case 328:
                        case 407: case 408:
                            treePanel.addObject(pRails,lineToAdd);
                            break;
                        //redstone
                         case 29: case 33: case 34: case 55: case 69:
                         case 70: case 72: case 76: case 77: case 123:
                         case 124: case 147: case 148: case 151: case 152:
                         case 331: case 404:
                            treePanel.addObject(pRedstone,lineToAdd);
                            break;
                        //Music
                        case 25: case 84: case 2257: case 2258: case 2559:
                        case 2260: case 2261: case 2267: case 2262:
                        case 2263: case 2264: case 2265: case 2266:       
                           treePanel.addObject(pMusic,lineToAdd);
                            break;              
                        //eggs
                        case 383:
                           treePanel.addObject(pEggs,lineToAdd);
                            break;              
                        //other    
                        default:      
                            treePanel.addObject(pOther, lineToAdd);
                            break;
                    }
                }
            }
            buff.close();
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
   private BufferedReader getBufferReader() throws IOException, MalformedURLException {
        String superurl = "http://pastebin.com/raw.php?i=Yc3NDtRW";
        URLConnection urlConn = getURLInformation(superurl);
        InputStreamReader inStream = new InputStreamReader(urlConn.getInputStream());
        BufferedReader buff = new BufferedReader(inStream);
        return buff;
    }

    private URLConnection getURLInformation(String superurl) throws IOException, MalformedURLException {
        URL url = new URL(superurl);
        URLConnection urlConn = url.openConnection();
        urlConn.setConnectTimeout(5000);
        urlConn.setReadTimeout(5000);
        return urlConn;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeItems = new DynamicTree();
        btnPurchase = new javax.swing.JButton();
        btnSell = new javax.swing.JButton();
        spnItemAmount = new javax.swing.JSpinner();
        chkStacks = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        btnRemoveItems = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblItems = new javax.swing.JTable();
        pnlBudget = new javax.swing.JPanel();
        txtBudgetBalance = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtBudgetTax = new javax.swing.JTextField();
        txtBudgetPurchased = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtBudgetSold = new javax.swing.JTextField();
        txtBudgetCurrent = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnClearBudget = new javax.swing.JButton();
 
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/jyncsed/gmail/Bundle"); // NOI18N
        setTitle(bundle.getString("MainFrame.title")); // NOI18N
        setBackground(new java.awt.Color(153, 180, 209));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(java.awt.Color.white);
        setIconImages(null);
        setMaximumSize(new java.awt.Dimension(800, 800));
        setPreferredSize(new java.awt.Dimension(1100, 715));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createEtchedBorder());

        //javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        //treeItems.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        //treeItems.setName(bundle.getString("MainFrame.treeItems.name")); // NOI18N
        //treeItems.setSelectionRows(null);
		populateTree(treeItems);
        jScrollPane2.setViewportView(treeItems);

        btnPurchase.setText(bundle.getString("MainFrame.btnPurchase.text")); // NOI18N
        btnPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPurchaseActionPerformed(evt);
            }
        });

        btnSell.setText(bundle.getString("MainFrame.btnSell.text")); // NOI18N
        btnSell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSellActionPerformed(evt);
            }
        });

        spnItemAmount.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));

        chkStacks.setText(bundle.getString("MainFrame.chkStacks.text")); // NOI18N
        chkStacks.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkStacks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkStacksActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText(bundle.getString("MainFrame.jLabel2.text")); // NOI18N

        btnRemoveItems.setText("<html>Delete Items</br>\nFrom List</html>");
        btnRemoveItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveItemsActionPerformed(evt);
            }
        });

        tblItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item", "Amount", "Purchase", "Sold"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblItems);
        tblItems.getColumnModel().getColumn(0).setResizable(false);
        tblItems.getColumnModel().getColumn(0).setHeaderValue(bundle.getString("MainFrame.tblItems.columnModel.title0")); // NOI18N
        tblItems.getColumnModel().getColumn(1).setResizable(false);
        tblItems.getColumnModel().getColumn(1).setHeaderValue(bundle.getString("MainFrame.jTable1.columnModel.title3")); // NOI18N
        tblItems.getColumnModel().getColumn(2).setResizable(false);
        tblItems.getColumnModel().getColumn(2).setHeaderValue(bundle.getString("MainFrame.jTable1.columnModel.title1")); // NOI18N
        tblItems.getColumnModel().getColumn(3).setResizable(false);
        tblItems.getColumnModel().getColumn(3).setHeaderValue(bundle.getString("MainFrame.jTable1.columnModel.title2")); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPurchase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chkStacks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(spnItemAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveItems, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                .addContainerGap())
        ); 
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spnItemAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkStacks)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPurchase)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSell)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveItems, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab(bundle.getString("MainFrame.jPanel2.TabConstraints.tabTitle"), jPanel2); // NOI18N

        pnlBudget.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        pnlBudget.setPreferredSize(new java.awt.Dimension(295, 191));

        txtBudgetBalance.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtBudgetBalance.setEnabled(false);
        txtBudgetBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBudgetBalanceActionPerformed(evt);
            }
        });

        jLabel6.setText(bundle.getString("MainFrame.jLabel6.text")); // NOI18N

        jLabel5.setText(bundle.getString("MainFrame.jLabel5.text")); // NOI18N

        txtBudgetTax.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtBudgetTax.setEnabled(false);
        txtBudgetTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBudgetTaxActionPerformed(evt);
            }
        });

        txtBudgetPurchased.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtBudgetPurchased.setEnabled(false);
        txtBudgetPurchased.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBudgetPurchasedActionPerformed(evt);
            }
        });

        jLabel4.setText(bundle.getString("MainFrame.jLabel4.text")); // NOI18N

        jLabel3.setText(bundle.getString("MainFrame.jLabel3.text")); // NOI18N

        txtBudgetSold.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtBudgetSold.setEnabled(false);
        txtBudgetSold.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBudgetSoldActionPerformed(evt);
            }
        });

        txtBudgetCurrent.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtBudgetCurrent.setText(bundle.getString("MainFrame.txtBudgetCurrent.text")); // NOI18N
        txtBudgetCurrent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBudgetCurrentActionPerformed(evt);
            }
        });
        txtBudgetCurrent.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBudgetCurrentKeyPressed(evt);
            }
        });

        jLabel1.setText(bundle.getString("MainFrame.jLabel1.text")); // NOI18N

        jLabel7.setText(bundle.getString("MainFrame.jLabel7.text")); // NOI18N

        btnClearBudget.setText(bundle.getString("MainFrame.btnClearBudget.text")); // NOI18N
        btnClearBudget.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearBudgetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBudgetLayout = new javax.swing.GroupLayout(pnlBudget);
        pnlBudget.setLayout(pnlBudgetLayout);
        pnlBudgetLayout.setHorizontalGroup(
            pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBudgetLayout.createSequentialGroup()
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBudgetLayout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel1))))
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBudgetCurrent)
                    .addComponent(txtBudgetSold)
                    .addComponent(txtBudgetPurchased)
                    .addComponent(txtBudgetTax)
                    .addComponent(txtBudgetBalance)
                    .addComponent(btnClearBudget, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlBudgetLayout.setVerticalGroup(
            pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBudgetLayout.createSequentialGroup()
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBudgetLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtBudgetCurrent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtBudgetSold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtBudgetPurchased, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBudgetTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBudgetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBudgetBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClearBudget)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(pnlBudget, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1043, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlBudget, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        getAccessibleContext().setAccessibleName(bundle.getString("MainFrame.AccessibleContext.accessibleName")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBudgetBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBudgetBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBudgetBalanceActionPerformed

    private void txtBudgetTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBudgetTaxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBudgetTaxActionPerformed

    private void txtBudgetPurchasedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBudgetPurchasedActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBudgetPurchasedActionPerformed

    private void txtBudgetSoldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBudgetSoldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBudgetSoldActionPerformed

    private void txtBudgetCurrentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBudgetCurrentActionPerformed
        // TODO add your handling code here:
        budget.changeCurrentTotal(Double.valueOf(this.txtBudgetCurrent.getText()));
        this.txtBudgetBalance.setText(String.valueOf(budget.calculateTotal()));
    }//GEN-LAST:event_txtBudgetCurrentActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowOpened

    private void txtBudgetCurrentKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBudgetCurrentKeyPressed
        // TODO add your handling code here:
        budget.changeCurrentTotal(Double.valueOf(this.txtBudgetCurrent.getText()));
        this.txtBudgetBalance.setText(String.valueOf(budget.calculateTotal()));
    }//GEN-LAST:event_txtBudgetCurrentKeyPressed

    private void btnClearBudgetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearBudgetActionPerformed
        // TODO add your handling code here:
        budget.clearValues();
        this.ClearTable();
        this.UpdateBudgetValues(budget.getBuyTotal(),budget.getSellTotal());
    }//GEN-LAST:event_btnClearBudgetActionPerformed

    private void btnRemoveItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveItemsActionPerformed
        // TODO add your handling code here:
        //JOptionPane.showMessageDialog(null,tblItems.getSelectedRow());
        int row = tblItems.getSelectedRow();
        double buyPrice = Double.valueOf(this.tblItems.getValueAt(row, 2).toString()) * -1;
        double sellPrice = Double.valueOf(this.tblItems.getValueAt(row, 3).toString()) * -1;
        this.UpdateBudgetValues(buyPrice, sellPrice);
        ((DefaultTableModel)tblItems.getModel()).removeRow(tblItems.getSelectedRow());
    }//GEN-LAST:event_btnRemoveItemsActionPerformed

    private void chkStacksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkStacksActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkStacksActionPerformed

    private void btnSellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSellActionPerformed
        // TODO add your handling code here:
        TableModel tableModel =  tblItems.getModel();
        int x = tableModel.getRowCount();
        String[][] newTableModel = GetCurrentTableItems(x, tableModel);
        int price = this.GetSellPrice() * this.GetItemAmount();
        AddNewDataToTable(newTableModel, x, this.GetItemName(), this.GetItemAmount(), 0, price);
        UpdateBudgetValues(Double.valueOf(0),Double.valueOf(price));
    }//GEN-LAST:event_btnSellActionPerformed

    private void btnPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPurchaseActionPerformed
        // TODO add your handling code here:
        TableModel tableModel =  tblItems.getModel();
        int x = tableModel.getRowCount();
        String[][] newTableModel = GetCurrentTableItems(x, tableModel);
        int price = this.GetBuyPrice() * this.GetItemAmount() * -1;
        AddNewDataToTable(newTableModel, x, this.GetItemName(), this.GetItemAmount(), price, 0);
        UpdateBudgetValues(Double.valueOf(price),Double.valueOf(0));
    }//GEN-LAST:event_btnPurchaseActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearBudget;
    private javax.swing.JButton btnPurchase;
    private javax.swing.JButton btnRemoveItems;
    private javax.swing.JButton btnSell;
    private javax.swing.JCheckBox chkStacks;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel pnlBudget;
    private javax.swing.JSpinner spnItemAmount;
    private javax.swing.JTable tblItems;
    private DynamicTree treeItems;
    private javax.swing.JTextField txtBudgetBalance;
    private javax.swing.JTextField txtBudgetCurrent;
    private javax.swing.JTextField txtBudgetPurchased;
    private javax.swing.JTextField txtBudgetSold;
    private javax.swing.JTextField txtBudgetTax;
    // End of variables declaration//GEN-END:variables

 
}
